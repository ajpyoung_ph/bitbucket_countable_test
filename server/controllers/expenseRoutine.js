
const mongojs = require('mongojs');
/*
	add module
	params:
	expenses - json data for the expenses
	{
		description:string,
		amount:number,
		date:string

	}
	returns - json data response
*/
const add = async function(expenses,res){
	console.log('exec add');
	console.log(expenses);
	let message = '';
	if(expenses.description && expenses.amount && expenses.date)
	{
		//breakdown date
		console.log('exec add - adding info');
		var dateArray = expenses.date.split('/');
		var dateInfo = new Date(dateArray[2],dateArray[0],dateArray[1]);
		let data = {
			description:expenses.description,
			date:dateInfo,
			amount:expenses.amount
		}
		console.log('exec add - insertOne');
		global.db.expRec.insert(data,function(err){
			if(err)
			{
				message = {
					type:'error',
					message:err,
					data:0
				};
				res.json(message);
				//return message;
			}else{
				message = {
					type:'success',
					message:"Data Saved",
					data:0
				};
				res.json(message);
				//return message;
			}
		});
	}else{
		console.log('exec add - return error');
		message = {
			type:'error',
			message:"Unknown expense form sent",
			data:0
		};
		res.json(message);
		//return message;
	}
}

/*
	edit module
	params:
	mongoID = record ID in string form
	returns - json data response
*/
const edit = async function(mongoID){
	let message = '';
	let findconst = {
		_id:mongojs.ObjectID(mongoID)
	};
	global.db.expRec.find(findconst, function(err,doc){
		if(err)
		{
			message = {
				type:'error',
				message:err,
				data:0
			};
			res.json(message);
			//return message;
		}else{
			message = {
				type:'success',
				message:'Data Found',
				data:1,
				dataSet:doc,
				dataType:typeof(doc)
			};
			res.json(message);
			//return message;
		}
	});
}

/*
	delete module
	params:
	mongoID = record ID in string form
	returns - json data response
*/
const del = async function(mongoID){
	let message = '';
	let findconst = {
		_id:mongojs.ObjectID(mongoID)
	};
	global.db.expRec.deleteOne(findconst, function(err,doc){
		if(err)
		{
			message = {
				type:'error',
				message:err,
				data:0
			};
			res.json(message);
			//return message;
		}else{
			message = {
				type:'success',
				message:'Data Deleted',
				data:0
			};
			//return message;
		}
	});
}


const requestData = require('./requestData');
/*
	read module
	params:
	requestJSON = data read request form
	{
		type:string, //single,week,month,year,last10
		identifier:number, //single - day of the month, week - week number of the year, month - month number, year - year number
		month:number, //single - month number
		year:number, //single - year number,week - year number, month - year number
	}
	returns - json data response
*/
const read = async function(requestJSON,res){
	let message = '';
	try{
		if(requestJSON.type)
		{
			let data;
			let type = requestJSON.type;
			switch(type.toUpperCase())
			{
				case 'SINGLE':
					requestData.single(requestJSON.identifier,requestJSON.month,requestJSON.year,res);
				case 'WEEK':
					requestData.week(requestJSON.identifier,requestJSON.year,res);
					break;
				case 'MONTH':
					requestData.month(requestJSON.identifier,requestJSON.year,res);
					break;
				case 'YEAR':
					requestData.year(requestJSON.identifier,res);
					break;
				case 'LAST10':
					requestData.last10(res);
					break;
				default:
					message = {
						type:'error',
						message:'Unknown Read Request',
						data:0
					};
					res.json(message);
					break;
			}
		}else{
			message = {
				type:'error',
				message:'Unknown requestJSON.type',
				data:0
			};
			res.json(message);
		}
	}catch(err){
		message = {
			type:'error',
			message:err,
			data:0
		};
		res.json(message);
	}
}

module.exports = {
	add:add,
	edit:edit,
	del:del,
	read:read
};