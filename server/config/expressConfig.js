const bodyParser = require('body-parser');
/*

	configuration routine for the Express module
	parameters:
	app - expressjs app declaration from start.js
	express - express constant from start.js
	return: none
*/

const conf =function(app, express){
	// Set our static directory for public assets like client scripts
	app.use(express.static('public'));

	// for parsing application/json
	app.use(bodyParser.json()); 

	// for parsing application/xwww-
	app.use(bodyParser.urlencoded({ extended: true })); 
	//form-urlencoded

	// Pretty prints HTML output
	app.locals.pretty = true;

	//CORS on ExpressJS
	app.use(function(req, res, next) {
	  res.header("Access-Control-Allow-Origin", "*");
	  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	  next();
	});
}

module.exports={
	conf:conf
};